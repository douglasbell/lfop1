var midiAccess,
    midiOutput,
    messageBanner,
    timeOffset = 0,
    midiListeners = [];

function LFOBox(settings) {
    var 
        div = settings.div,
        on = settings.on || true,
        waveType = settings.waveType || 'sine',
        frequency = settings.frequency || 1,
        amplitude = settings.amplitude || 63.50,
        waveLength,
        waveFuncs,
        activeWaveFunc,
        lastRandomValue,
        lastTimeRandom = 0,
        timeOffset = 0,
        waveData = [],
        lastAddTime,
        syncOnNote = true,

        // Elements
        ccLabel = div.getElementsByClassName('cc-label')[0],
        onOffWrapper = div.getElementsByClassName('on-off')[0],
        onButton = div.getElementsByClassName('on-button')[0],
        offButton = div.getElementsByClassName('off-button')[0],
        canvasWrapper = div.getElementsByClassName('canvas-wrapper')[0],
        waveCanvas = div.getElementsByClassName('wave-canvas')[0],
        noiseButton = div.getElementsByClassName('noise-button')[0],
        waveSelect = div.getElementsByClassName('wave-select')[0],
        frequencyInput = div.getElementsByClassName('frequency input')[0],
        amplitudeInput = div.getElementsByClassName('amplitude input')[0],
        syncOnOffWrapper = div.getElementsByClassName('sync on-off')[0],
        yesButton = div.getElementsByClassName('yes-button')[0],
        noButton = div.getElementsByClassName('no-button')[0],

        context = waveCanvas.getContext('2d');

    waveFuncs = {
        'sine': function (step) {return Math.sin(step * Math.PI * 2)},
        'square': function (step) {return step < 0.5 ? 1 : -1},
        'sawtooth': function (step) {return 2 * (step - Math.round(step))},
        'reverse-sawtooth': function (step) {return 2 * (1 - step) - 1},
        'triangle': function (step) {return Math.abs(Math.round(step) - step) * 4 - 1},
        'noise': function () {
            if (!lastTimeRandom || getTime() > lastTimeRandom + (1000/frequency)) {
                lastTimeRandom = getTime();

                lastRandomValue = (Math.random() * 2 - 1);
            }

            return lastRandomValue;
        }
    };

    function draw() {
        var x, y;

        context.clearRect(0, 0, waveLength, 200);

        context.beginPath();
        context.lineWidth = 5;
        context.lineCap = 'round';
        context.lineJoin = 'round';
        context.strokeStyle = '#333333';

        for (x = 0; x <= waveLength - 6; x += 1) {

            y = (waveData[x] || 0) + 100;

            if (x == 0) {
                context.moveTo(x+3, y);
            }
            else {
                context.lineTo(x+3, y);
            }
        }
        context.stroke();

        requestAnimationFrame(draw);
    }

    function sendCC() {
        var step,
            value;

        if (on) {
            step = (getTime() % (1000 / frequency)) / (1000 / frequency);
            value = activeWaveFunc(step) * amplitude;

        }
        else {
            value = 0;
        }

        if (midiOutput) {
            midiOutput.send([0xB0, settings.ccNumber, value + (128/2)]);
        }

        if (Math.round(getTime()) != lastAddTime) {
            waveData.unshift(value);
            waveData.length = 1000;

            lastAddTime = Math.round(getTime());
        }

        setTimeout(sendCC, 0);
    }

    function getInputValues() {
        frequency = frequencyInput.value;
        amplitude = amplitudeInput.value;
    }

    function getTime() {
        if (syncOnNote) return performance.now() - timeOffset;
        else return performance.now();
    }

    // Initialization
    activeWaveFunc = waveFuncs['sine'];

    midiListeners.push(function (event) {
        switch (event.data[0] & 0xf0) {
            case 0x90:
                if (event.data[2]!=0) {  // if velocity != 0, this is a note-on message
                    timeOffset = performance.now();
                    lastTimeRandom = 0;
                }
        }
    });

    setTimeout(function () {
        waveCanvas.width = waveLength = canvasWrapper.offsetWidth;
    }, 100);

    new luk.Knob(
        div.getElementsByClassName('frequency knob')[0],
        {
            'value': 1,
            'minimumValue': .1,
            'maximumValue': 50,
            'interval': .1,
            'onchange':
            function (value) {
                frequencyInput.value = frequency = value.toFixed(2);
            }
        }
    );

    new luk.Knob(
        div.getElementsByClassName('amplitude knob')[0],
        {
            'value': 63.5,
            'minimumValue': 0,
            'maximumValue': 63.5,
            'interval': .2,
            'onchange':
            function (value) {
                amplitudeInput.value = amplitude = value.toFixed(2);
            }
        }
    );

    waveSelect.addEventListener('change', function () {
        var func = waveFuncs[waveSelect.value];

        if (func) {
            activeWaveFunc = func;
        }
    });

    onOffWrapper.addEventListener('click', function (event) {
        event.preventDefault();

        on = !on;

        if (on) {
            onButton.className = 'glyphicon glyphicon-plus-sign on-button';
            offButton.className = 'glyphicon glyphicon-minus-sign dim off-button';
        }
        else {
            onButton.className = 'glyphicon glyphicon-plus-sign dim on-button';
            offButton.className = 'glyphicon glyphicon-minus-sign off-button';
        }
    });

    syncOnOffWrapper.addEventListener('click', function (event) {
        event.preventDefault();

        syncOnNote = !syncOnNote;

        if (syncOnNote) {
            yesButton.className = 'glyphicon glyphicon-ok-circle yes-button';
            noButton.className = 'glyphicon glyphicon-ban-circle dim yes-button';
        }
        else {
            yesButton.className = 'glyphicon glyphicon-ok-circle dim yes-button';
            noButton.className = 'glyphicon glyphicon-ban-circle no-button';
        }
    });

    frequencyInput.addEventListener('change', getInputValues);
    frequencyInput.addEventListener('keyup', getInputValues);
    amplitudeInput.addEventListener('change', getInputValues);
    amplitudeInput.addEventListener('keyup', getInputValues);


    div.className = 'lfo-box ' + settings.class;
    ccLabel.innerHTML = 'CC' + settings.ccNumber;
    frequencyInput.value = frequency;
    amplitudeInput.value = amplitude;

    sendCC();
    draw();
}

function showBanner(text) {
    messageBanner.innerHTML = text;
    messageBanner.style.display = 'block';
}

function hideBanner() {
    messageBanner.style.display = 'none';
}

function setupMIDIInputOutput() {
    midiAccess.outputs.forEach(function (output) {
        if (output.name.indexOf('OP-1') > -1) {
            midiOutput = output;
        }
    });

    midiAccess.inputs.forEach(function (input) {
        if (input.name.indexOf('OP-1') > -1) {
            input.onmidimessage = function (event) {
                midiListeners.forEach(function (listener) {
                    listener(event);
                });
            };
        }
    });

    if (!midiOutput) {
        showBanner('<i class="glyphicon glyphicon-thumbs-down"></i> <span class="message-text">Not connected to the OP-1</span>');
    }
    else {
        hideBanner();
        showBanner('<i class="glyphicon glyphicon-thumbs-up"></i> <span class="message-text">Connected to the OP-1</span>');
    }
}

function onMIDIInit(midi) {
    midiAccess = midi;
    setupMIDIInputOutput();
    midiAccess.onstatechange = setupMIDIInputOutput;
}

function onMIDIReject(err) {
    showBanner('<i class="glyphicon glyphicon-thumbs-down"></i> <span class="message-text">The MIDI system failed to start.</span>');
}

var knobs = document.querySelector('#knobs:not([data-animated])');
if (knobs) {
    var e = knobs.querySelectorAll('path'),
    rotate = function(knobs) {
        e[knobs].style.transform='rotate(' + 360*Math.random() + 'deg)'
    };
    setInterval( function() {
        rotate (Math.floor(Math.random() * e.length))
    }, 1111);
    for (var r = 0; r < e.length; r++)
        rotate(r);
    knobs.setAttribute('data-animated', !0)
}!function (t, e) {
    function rotate (t, e) {
        for (var n = 0; n < c; n++)
            if (t > l[n][0][0] && t<l[n][1][0] && e > l[n][0][1] && e < l[n][1][1])
                return n + 53;
        return -1
    }
}

// Setup
window.addEventListener('load', function () {
    var template = document.getElementById('lfo-box-template').innerHTML,
        settingsObjects = [
            {'ccNumber': 1, 'class': 'blue'},
            {'ccNumber': 2, 'class': 'green'},
            {'ccNumber': 3, 'class': 'white'},
            {'ccNumber': 4, 'class': 'orange'}
        ];

    messageBanner = document.getElementById('message-banner');

    if (!window.performance) {
        window.performance = {
            'now': function () {
                return (new Date()).getTime();
            }
        };
    }

    window.addEventListener('mousemove', function (event) {
        event.preventDefault();
    });

    if (navigator.requestMIDIAccess) {
        navigator.requestMIDIAccess().then(onMIDIInit, onMIDIReject);
    }
    else {
        showBanner('<i class="glyphicon glyphicon-thumbs-down"></i> <span class="message-text">Browser does not support MIDI</span>');
    }

    settingsObjects.forEach(function (settings) {
        var div = settings.div = document.getElementById(settings.ccNumber);
        div.innerHTML = template;
        setTimeout(function () {new LFOBox(settings)}, 0);
    });
});

// Luk
var luk;

function extend(target, source) {
    var key;

    for (key in source) {
        target[key] = source[key];
    }
}

luk = {
    'Knob': function (element, options) {
        var self = this,
            defaults,
            //classes,
            innerElement,
            tickElement,
            downX,
            downY,
            downValue;

        defaults = {
            'value': 0,
            'minimumValue': 0,
            'maximumValue': 100,
            'minimumDegree': -180,
            'maximumDegree': 90,
            'interval': 1
        }

        extend(self, defaults);

        if (options) {
            extend(self, options);
        }

        if (element) {
            self.element = element;
        }
        else {
            self.element = document.createElement('div');
        }

        //classes = self.element.className;
        //self.element.className = classes + ' knob';

        innerElement = document.createElement('div');
        innerElement.className = 'inner';

        tickElement = document.createElement('div');
        tickElement.className = 'tick';

        self.element.appendChild(innerElement);
        innerElement.appendChild(tickElement);

        function position() {
            var valueRange = self.maximumValue - self.minimumValue,
                degreeRange = self.maximumDegree - self.minimumDegree,
                tickSize = degreeRange / valueRange,
                rotation = self.minimumDegree + (tickSize * self.value);

            innerElement.style.webkitTransform = 'rotate(' + rotation + 'deg)';
        }

        function mouseMove(event) {
            var x = event.pageX,
                y = event.pageY,
                distance = ((x - downX) + (downY - y)) / 2;

            self.value = downValue + (distance * self.interval);

            if (self.value < self.minimumValue) {
                self.value = self.minimumValue;
            }
            if (self.value > self.maximumValue) {
                self.value = self.maximumValue;
            }

            if (self.onchange) {
                self.onchange(self.value);
            }

            position();
        }

        function mouseUp() {
            document.removeEventListener('mousemove', mouseMove);
            document.removeEventListener('mouseup', mouseUp);
        }

        function mouseDown(event) {
            event.preventDefault();
            downX = event.pageX;
            downY = event.pageY;
            downValue = self.value;

            document.addEventListener('mousemove', mouseMove);
            document.addEventListener('mouseup', mouseUp);
        }

        self.element.addEventListener('mousedown', mouseDown);

        position();
    }
};
